<?php
require('Animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("Shaun");
echo "Name: $sheep->name <br>"; // "shaun"
echo "Legs: $sheep->legs <br>"; // 4
echo "Cold Blooded: $sheep->cold_blooded <br><br>"; // "no"

$kodok = new Frog("Buduk");
echo "Name: $kodok->name <br>";
echo "Legs: $kodok->legs <br>";
echo "Cold Blooded: $kodok->cold_blooded <br>";
echo "Jump: " . $kodok->jump() . "<br><br>";

$sungokong = new Ape("Kera sakti");
echo "Name: $sungokong->name <br>";
echo "Legs: $sungokong->legss <br>";
echo "Cold Blooded: $kodok->cold_blooded <br>";
echo "Jump: " . $sungokong->yell();

?>